// Range
function range(startNum, finishNum)

{
    if (startNum == null || finishNum == null)
    {
        return -1;
    }

else if (startNum < finishNum)
    {
    var hasil = []
        for (i=startNum; i<=finishNum; i++)
        {
            hasil.push (i)
        }
            return hasil;
    }

    else if (startNum > finishNum)
    {
    var hasil = []
        for (i=startNum; i>finishNum; i--)
        {
            hasil.push (i)
        }
            return hasil;
    }

}
console.log(range(1, 10))
console.log(range(1))
console.log(range(11,18))
console.log(range(54, 50))
console.log(range())

//rangeWithStep
function rangeWithStep(startNum, finishNum, step)

{
    if (startNum == null || finishNum == null)
    {
        return -1;
    }

        else if (startNum < finishNum)
        {
            var hasil = []
                for (i=startNum; i<=finishNum; i+=step)
        {
            hasil.push (i)
        }
            return hasil;       
}

else if (startNum > finishNum)
{
    var hasil = []
        for (i=startNum; i>finishNum; i-=step)
        {
            hasil.push (i)
        }
            return hasil;
        }
}
console.log(rangeWithStep(1, 10, 2)) 
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))

//sum of range

function sum(startNum, finishNum, deret)

    {
        if (deret == null)
    {
        deret = 1
    }

        if (startNum == null && finishNum == null)
        {
            return 0;
        }

    else if (finishNum == null)
    {
        return 1;
    }

    else if (startNum < finishNum)
    {
        var hasil = 0
            for (i=startNum; i<=finishNum; i+=deret)
            {
                hasil = hasil + i
            }
                return hasil;
    }

    else if (startNum > finishNum)
    {
        var hasil = 0
            for (i=startNum; i>=finishNum; i-=deret)    
            {
                hasil = hasil + i
            }
                return hasil;
    }
}

console.log(sum(1,10)) 
console.log(sum(5, 50, 2)) 
console.log(sum(15,10)) 
console.log(sum(20, 10, 2)) 
console.log(sum(1)) 
console.log(sum())

//array multidimensi

function dataHandling(input)

    {
        var n=input.length
            for (i=0; i<n; i++)
            {
                var nn=input[i].length
                console.log("Nomor ID : "+input[i][0])
                console.log("Nama Lengkap : "+input[i][1])
                console.log("TTL : "+input[i][2])
                console.log("Hobi : "+input[i][3])
                console.log("")
            }
    }

var input = [
["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989","Membaca"],
["0002", "Dika Sembiring", "Medan", "10/10/1992", "BermainGitar"],
["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
["0004", "Bintang Senjaya", "Martapura", "6/4/1970","Berkebun"]
]
console.log(dataHandling(input))

// Balik Kata

    function balikKata(kata)
    {
        var katabaru = " "
            for (i=(kata.length-1); i>=0; i--)
            {
                katabaru = katabaru+kata[i];    
            }
                return katabaru;
    }
    
console.log(balikKata("Kasur Rusak")) 
console.log(balikKata("Informatika")) 
console.log(balikKata("Haji Ijah")) 
console.log(balikKata("racecar")) 
console.log(balikKata("I am Humanikers" ))

